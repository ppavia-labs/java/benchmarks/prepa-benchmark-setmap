package main.java.ppa.labs.oca.stream;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import main.java.ppa.labs.oca.stream.model.Actor;
import main.java.ppa.labs.oca.stream.model.Decade;

public class OcaStream {
	private static final String className	= OcaStream.class.getSimpleName();
	public static Decade decade;

	public void initializeDecade () {
		decade	= Decade.build(1, 10, 2010);
	}

	
	public static void log(String methodName, String valueName, String value ) {
		Instant instantNow = Instant.now();
		System.out.println("[" + instantNow + "]" + "[" + className + "]" + "[" + methodName + "]" + "[" + valueName + "]" + " - " + value);
	}
	
	public List<Integer> mapReduceV1 (Decade decade) {
		Optional<List<Integer>> optionAges = decade.movies.stream()
		.map(movie -> {
			return movie.actors.stream()
					.map(actor -> {
						return actor.age;
					})
					.distinct()
					.sorted()
					.collect(Collectors.toList());
		})
		.reduce((l1, l2) -> {
			l1.addAll(l2);
			return l1;
		});
		
		return optionAges
    	.get()
    	.parallelStream()
    	.distinct()
    	.collect(Collectors.toList());
	}
	
	public List<Integer> mapReduceV2 (Decade decade) {
		List<Integer> ages	= new ArrayList<Integer>();
		decade.movies.stream()
		.forEach(movie -> {
			movie.actors.stream()
					.forEach(actor -> {
						if (!ages.contains(actor.age)) {
							ages.add(actor.age);
						}
					});
		});
		return ages;
	}
	
	public Set<Actor> collectToSet (Decade decade) {
		List<Integer> ages	= new ArrayList<Integer>();
		
		return decade.movies.stream()
		.map(movie -> {
			return movie.actors.parallelStream()
			.map(actor -> {
				return actor;
			})
			.filter(actor  -> {
				if ( !ages.contains(actor.age) ) {
					ages.add(actor.age);
					return true;
				}
				return false;
			})
			.collect(Collectors.toSet());
		})
		.reduce(new HashSet<Actor>(), (s1,s2) -> {
			s1.addAll(s2);
			return s1;
		});
	}
	
	public Set<Actor> collectToMapThenSet (Decade decade) {		
		Map<Integer, Actor> mapActors =  decade.movies.stream()
		.map(movie -> {
			return movie.actors.parallelStream()
			.map(actor -> actor)
			.collect(Collectors.toMap(Actor::getAge, Function.identity(), (existing, replacement) -> existing));
		})
		.reduce(new HashMap<Integer, Actor>(), (m1,m2) -> {
			m2.keySet().parallelStream()
			.forEach(age -> {
				if ( !m1.containsKey(age) ) {
					m1.put(age, m2.get(age));
				}
			});
			return m1;
		});
		
		return new HashSet<Actor>(mapActors.values());
	}

}

