package main.java.ppa.labs.oca.stream.model;

public class Actor {
	public String firstName;
	public String lastName;
	public int age;
	
	Actor (
			String firstName,
			String lastName,
			int age
			) {
		this.firstName 	= firstName;
		this.lastName 	= lastName;
		this.age 		= age;
	}
	
	public int getAge() {
		return this.age;
	}
}
