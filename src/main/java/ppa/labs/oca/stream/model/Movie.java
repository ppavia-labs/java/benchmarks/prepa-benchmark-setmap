package main.java.ppa.labs.oca.stream.model;

import java.util.ArrayList;
import java.util.List;

public class Movie {
	public String title;
	public List<Actor> actors	= new ArrayList<Actor>();
	private Movie() {}
	
	private Movie(String title) {
		this.title = title;
	}
	
	static public Movie build (String title, List<Actor> actors) {
		return MovieBuilder.buildMovie(title, actors);
	}
	static private class MovieBuilder {
		static Movie buildMovie (String title, List<Actor> actors) {
			Movie movie	= new Movie(title);
			
			movie.actors.addAll(actors);
			
			return movie;
		}
	}
}


