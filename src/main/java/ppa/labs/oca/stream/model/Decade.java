package main.java.ppa.labs.oca.stream.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Decade {
	int year;
	public List<Movie> movies	= new ArrayList<Movie>();
	
	Decade (int year) {
		this.year	= year;
	}
	
	static public Decade build (int nbMovies, int nbActors, int year) {
		return DecadeBuilder.buildDecade(nbMovies, nbActors, year);
	}
	
	static private class DecadeBuilder {
		static Decade buildDecade (int nbMovies, int nbActors, int year) {
			Decade decade = new Decade(year);
			for (int j=0; j<nbMovies; j++) {
				List<Actor> lsActors	= new ArrayList<Actor>();
				
				for (int i=0; i<nbActors; i++) {
					int rdInt	= ThreadLocalRandom.current().nextInt(20, 88 + 1);
					Actor actor	= new Actor (
							"actorFirstName"+rdInt,
							"actorLastName"+rdInt,
							rdInt
							);
					lsActors.add(actor);
				}
				decade.movies.add(Movie.build("title"+j, lsActors));			
			}
			
			return decade;
		}
	}
}

