package main.java.ppa.labs.oca;

import java.time.Instant;
import java.util.Set;

import main.java.ppa.labs.oca.stream.OcaStream;
import main.java.ppa.labs.oca.stream.model.Actor;

public class CertifMain {
	private static final String className	= CertifMain.class.getSimpleName();
	static OcaStream ocaStream				= new OcaStream();

	public static void main (String... args) {
		String methodName = new Object() {}
		.getClass()
		.getEnclosingMethod()
		.getName();
		
		ocaStream.initializeDecade();
		

		int i = 0;
		while (i<args.length) {
			log(methodName, "args", args[i]);
			i++;
		}
		
		OcaStream ocaStream	= new OcaStream();
		// control
		OcaStream.decade.movies.parallelStream()
		.forEach(movie -> {
			movie.actors.parallelStream()
			.forEach(actor -> {
				log(methodName, "actor : ", String.valueOf(actor.age) + " - " + actor.lastName);
			});
		});
		
		
		Set<Actor> setActors1	= ocaStream.collectToMapThenSet(OcaStream.decade);
		setActors1.stream()
		.sorted((a1, a2) -> {
			return a1.age - a2.age;
		})
		.forEach(actor -> {
			log(methodName, "collectToMapThenSet sorted", String.valueOf(actor.age));
		});
		log(methodName, "------------------------", "---------------------");	
		
		Set<Actor> setActors2	= ocaStream.collectToSet(OcaStream.decade);
		setActors2.stream()
		.sorted((a1, a2) -> {
			return a1.age - a2.age;
		})
		.forEach(actor -> {
			log(methodName, "collectToSet sorted", String.valueOf(actor.age));
		});
	}
	
	public static void log(String methodName, String valueName, String value ) {
		Instant instantNow = Instant.now();
		System.out.println("[" + instantNow + "]" + "[" + className + "]" + "[" + methodName + "]" + "[" + valueName + "]" + " - " + value);
	}
}
